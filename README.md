# Examen Final - Programación III
### Ing. y Lic. en Sistemas
### Turno Noviembre-Diciembre 2023, 1° llamado

### Objetivos
- Desarrollar endpoints que permitan el registro y consulta de la entidad Mascota
- Implementar Frontend que consuma los endpoints desarrollados

### Tiempo
- 2 horas reloj
### Evaluacion
- Se evaluará la versión del proyecto en el repositorio correspondiente, a la hora de finalización del examen, estimada para el día 30/11/2023 17:00
- El proyecto debe compilar sin errores en cualquier entorno de programación en el que se abra
- Todos los test unitarios deben pasar en verde
- Se probará la funcionalidad desde el Frontend

### Punto de partida
- Se proveerá el esquema de Backend y Frontend parcialmente desarrollado, de manera que el alumno pueda lograr los objetivos en el tiempo previsto.

## Consigna
#### Módulo Veterinaria
_Se desea implementar un backend con su respectivo frontend para un microservicio que permita registrar mascotas._

#### Restricciones:
- No puede existir dos Mascotas con el mismo nombre
- Todos los atributos de Mascota son obligatorios
- La fecha de nacimiento de la mascota no puede ser superior a la actual

#### Funcionalidad
- Crear Mascota
  - Endpoint: POST http://localhost:8080/mascotas
  - RequestBody:
    ```json
    {
      "id": null,
      "nombre": "Max",
      "fecha_nacimiento": "2021-01-01T10:00:00.000Z"
    }
    ```

- Buscar Mascotas
  - Endpoint: GET http://localhost:8080/mascotas

#### Buenas prácticas y conceptos a considerar
- La nomenclatura de paquetes será en minúsculas
- La nomenclatura de clases será en UpperCamelCase
- La nomenclatura de métodos será en lowerCamelCase
- La organización de paquetes será por modelo->aspecto, tanto a nivel src/main como a nivel src/test. Ejemplo:
  ```
  mascotas
  └─ excepciones
  └─ modelo
  └─ repositorio
  └─ casodeuso
  ```
- Usar Excepciones personalizadas
- Se debe usar método factory/instancia para crear objetos
- Nomenclatura representativa de clases, métodos, etc.
