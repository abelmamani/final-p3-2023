import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Mascota} from "../../interfaces/mascota.interface";
import {MascotaService} from "../../services/mascota.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-mascota',
  templateUrl: './mascota.component.html',
  styleUrls: ['./mascota.component.css']
})
export class MascotaComponent implements OnInit {

  miFormulario: FormGroup = this.fb.group({
    nombre: [undefined, [Validators.required, Validators.minLength(3)]],
    fecha_nacimiento: [undefined, [Validators.required]],
  });

  constructor(private fb: FormBuilder,
              private matSnackBar: MatSnackBar,
              private mascotaService: MascotaService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.miFormulario.reset({});
    this.miFormulario.untouched;
  }

  guardar() {
    if(this.miFormulario.valid){
      this.mascotaService.guardarMascota(this.miFormulario.value).subscribe({
        next: (res)=>{
          this.showSnackBar("se registro nueva mascota");
          this.router.navigate(["mascotas/listado"]);
        },
        error: (err) => {
          this.showSnackBar(err.error ? err.error.message : "hubo un error al registrar!");
        }
      });
    }else{
      this.miFormulario.touched;
    }
  }

  showSnackBar(mensaje: string) {
    this.matSnackBar.open(mensaje, 'Aceptar', {
      duration: 3000
    });
  }

  cancelar() {
    this.showSnackBar("accion cancelada!");
    this.router.navigate(["mascotas/listado"]);
  }
}
