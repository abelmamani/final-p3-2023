import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Mascota} from "../../interfaces/mascota.interface";
import {MascotaService} from "../../services/mascota.service";
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {
  displayedColumns: string[] = ['id', 'nombre', 'fecha_nacimiento'];
  mascotas: Mascota[] = [];

  constructor(private router: Router,
              private mascotaService: MascotaService, private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.getMascotas();
  }
  getMascotas(){
    this.mascotaService.getMascotas().subscribe({
      next: (res: Mascota[]) => {
        this.mascotas = res;
      },
      error: (err) => {
        this.snackBar.open("Aceptas", "Error al obtener el listado!", {duration: 3000});
      }
    });
  }

  addData() {
    this.router.navigate(["mascotas/mascota"]);
  }
}
