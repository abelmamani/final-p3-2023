import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Mascota} from "../interfaces/mascota.interface";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MascotaService {

  private baseUrl: string = environment.baseUrl;

  constructor(private httpClient: HttpClient) {
  }

  guardarMascota(mascota: Mascota): Observable<any> {
    return this.httpClient.post(this.baseUrl+"/mascotas", mascota);
  }

  getMascotas():Observable<Mascota[]> {
    return this.httpClient.get<Mascota[]>(this.baseUrl+"/mascotas");
  }
}
