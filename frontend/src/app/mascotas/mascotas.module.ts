import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListadoComponent} from "./pages/listado/listado.component";
import {MascotaComponent} from "./pages/mascota/mascota.component";
import {PrincipalComponent} from './pages/principal/principal.component';
import {MaterialModule} from "../material/material.module";
import {MascotasRoutingModule} from "./mascotas-routing.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [
    ListadoComponent,
    MascotaComponent,
    PrincipalComponent
  ],
  imports: [
    CommonModule,
    MascotasRoutingModule,
    FlexLayoutModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class MascotasModule {
}
