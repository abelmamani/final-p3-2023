import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {PrincipalComponent} from "./pages/principal/principal.component";
import {ListadoComponent} from "./pages/listado/listado.component";
import {MascotaComponent} from "./pages/mascota/mascota.component";

const routes: Routes = [
  {
    path: '',
    component: PrincipalComponent,
    children: [
      {
        path: 'listado',
        component: ListadoComponent
      },
      {
        path: 'mascota',
        component: MascotaComponent
      },
      {
        path: '**',
        redirectTo: 'listado'
      }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class MascotasRoutingModule {
}
