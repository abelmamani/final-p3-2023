package ar.edu.undec.adapter.data.mascota.repoimplementacion;

import ar.edu.undec.adapter.data.exception.DataBaseException;
import ar.edu.undec.adapter.data.mascota.crud.ICrearMascotaCRUD;
import ar.edu.undec.adapter.data.mascota.mapper.MascotaDataMapper;
import mascota.modelo.Mascota;
import mascota.output.ICrearMascotaRepositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrearMascotaRepositorioImplementacion implements ICrearMascotaRepositorio {
    private ICrearMascotaCRUD crearMascotaCRUD;
    @Autowired
    public CrearMascotaRepositorioImplementacion(ICrearMascotaCRUD crearMascotaCRUD) {
        this.crearMascotaCRUD = crearMascotaCRUD;
    }

    @Override
    public boolean existePorNombre(String nombre) {
        return crearMascotaCRUD.existsByNombre(nombre);
    }

    @Override
    public Integer guardar(Mascota mascota) {
        try {
            return crearMascotaCRUD.save(MascotaDataMapper.dataEntityMapper(mascota)).getId();
        }catch (Exception exception){
            throw new DataBaseException("error in database");
        }
    }
}
