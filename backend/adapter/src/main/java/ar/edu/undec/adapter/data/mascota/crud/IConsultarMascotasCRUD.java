package ar.edu.undec.adapter.data.mascota.crud;

import ar.edu.undec.adapter.data.mascota.model.MascotaEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface IConsultarMascotasCRUD extends CrudRepository<MascotaEntity, Integer> {
    Collection<MascotaEntity> findAll();
}
