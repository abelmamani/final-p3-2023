package ar.edu.undec.adapter.service.mascota.controller;

import ar.edu.undec.adapter.service.mascota.mapper.MascotaDTODataMapper;
import ar.edu.undec.adapter.service.mascota.model.MascotaDTO;
import mascota.input.ICrearMascotaInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("mascotas")
@CrossOrigin("*")
public class CrearMascotaController {
    private ICrearMascotaInput iCrearMascotaInput;
    @Autowired
    public CrearMascotaController(ICrearMascotaInput iCrearMascotaInput) {
        this.iCrearMascotaInput = iCrearMascotaInput;
    }
    @PostMapping
    public ResponseEntity<?> crearMascota(@RequestBody MascotaDTO mascota){
        try{
            return ResponseEntity.created(null).body(iCrearMascotaInput.crearMascota(MascotaDTODataMapper.dataCoreMapper(mascota)));
        }catch (RuntimeException exception){
            Map<String,String> response = new HashMap<>();
            response.put("message", exception.getMessage());
            return ResponseEntity.badRequest().body(response);
        }
    }
}
