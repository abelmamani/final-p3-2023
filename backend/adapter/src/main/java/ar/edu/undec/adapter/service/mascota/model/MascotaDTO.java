package ar.edu.undec.adapter.service.mascota.model;

import java.time.LocalDate;

public class MascotaDTO {
    private Integer id;
    private String nombre;
    private LocalDate fecha_nacimiento;
    public MascotaDTO(){}
    public MascotaDTO(Integer id, String nombre, LocalDate fecha_nacimiento) {
        this.id = id;
        this.nombre = nombre;
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public LocalDate getFecha_nacimiento() {
        return fecha_nacimiento;
    }
}
