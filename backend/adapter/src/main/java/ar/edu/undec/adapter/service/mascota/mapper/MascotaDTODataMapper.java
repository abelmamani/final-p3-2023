package ar.edu.undec.adapter.service.mascota.mapper;

import ar.edu.undec.adapter.data.exception.FailedMapperException;
import ar.edu.undec.adapter.data.mascota.model.MascotaEntity;
import ar.edu.undec.adapter.service.mascota.model.MascotaDTO;
import mascota.modelo.Mascota;

public class MascotaDTODataMapper {
    public static Mascota dataCoreMapper(MascotaDTO mascota){
        try {
            return Mascota.instancia(mascota.getId(),
                    mascota.getNombre(),
                    mascota.getFecha_nacimiento());
        }catch (Exception exception){
            throw new FailedMapperException("error mapping from dto to core");
        }
    }
    public static MascotaDTO dataDTOMapper(Mascota mascota){
        try {
            return new MascotaDTO(mascota.getId(),
                    mascota.getNombre(),
                    mascota.getFechaNacimiento());
        }catch (Exception exception){
            throw new FailedMapperException("error mapping from core to dto");
        }
    }
}
