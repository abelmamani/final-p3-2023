package ar.edu.undec.adapter.data.mascota.mapper;

import ar.edu.undec.adapter.data.exception.FailedMapperException;
import ar.edu.undec.adapter.data.mascota.model.MascotaEntity;
import mascota.modelo.Mascota;

public class MascotaDataMapper {
    public static Mascota dataCoreMapper(MascotaEntity mascota){
        try {
            return Mascota.instancia(mascota.getId(),
                    mascota.getNombre(),
                    mascota.getFechaNacimiento());
        }catch (Exception exception){
            throw new FailedMapperException("error mapping from entity to core");
        }
    }
    public static MascotaEntity dataEntityMapper(Mascota mascota){
        try {
            return new MascotaEntity(mascota.getId(),
                    mascota.getNombre(),
                    mascota.getFechaNacimiento());
        }catch (Exception exception){
            throw new FailedMapperException("error mapping from core to entity");
        }
    }
}
