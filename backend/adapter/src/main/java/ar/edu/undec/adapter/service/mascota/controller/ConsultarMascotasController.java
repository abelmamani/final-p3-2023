package ar.edu.undec.adapter.service.mascota.controller;

import ar.edu.undec.adapter.service.mascota.mapper.MascotaDTODataMapper;
import ar.edu.undec.adapter.service.mascota.model.MascotaDTO;
import mascota.input.IConsultarMascotasInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("mascotas")
@CrossOrigin("*")
public class ConsultarMascotasController {
    private IConsultarMascotasInput iConsultarMascotasInput;
    @Autowired
    public ConsultarMascotasController(IConsultarMascotasInput iConsultarMascotasInput) {
        this.iConsultarMascotasInput = iConsultarMascotasInput;
    }
    @GetMapping
    public ResponseEntity<List<MascotaDTO>> consultarMascotas(){
        List<MascotaDTO> mascotas = iConsultarMascotasInput.consultarMascotas().stream().map(MascotaDTODataMapper::dataDTOMapper).collect(Collectors.toList());
        if(!mascotas.isEmpty())
            return ResponseEntity.ok(mascotas);
        else
            return ResponseEntity.noContent().build();
    }
}
