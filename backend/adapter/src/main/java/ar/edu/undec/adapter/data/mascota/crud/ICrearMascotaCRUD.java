package ar.edu.undec.adapter.data.mascota.crud;

import ar.edu.undec.adapter.data.mascota.model.MascotaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICrearMascotaCRUD extends CrudRepository<MascotaEntity, Integer> {
    boolean existsByNombre(String nombre);
}
