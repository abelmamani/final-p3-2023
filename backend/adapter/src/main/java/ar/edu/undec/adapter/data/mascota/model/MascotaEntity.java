package ar.edu.undec.adapter.data.mascota.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "mascotas")
public class MascotaEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String nombre;
    @Column(name = "fecha_nacimiento")
    private LocalDate fechaNacimiento;

    public MascotaEntity(){}
    public MascotaEntity(Integer id, String nombre, LocalDate fechaNacimiento) {
        this.id = id;
        this.nombre = nombre;
        this.fechaNacimiento = fechaNacimiento;
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }
}
