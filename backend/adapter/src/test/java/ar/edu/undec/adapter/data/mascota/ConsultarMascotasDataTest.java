package ar.edu.undec.adapter.data.mascota;

import ar.edu.undec.adapter.data.mascota.crud.IConsultarMascotasCRUD;
import ar.edu.undec.adapter.data.mascota.repoimplementacion.ConsultarMascotasRepositorioImplementacion;
import ar.edu.undec.adapter.factory.FactoryMascotaAdapter;
import mascota.modelo.Mascota;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ConsultarMascotasDataTest {
    @InjectMocks
    ConsultarMascotasRepositorioImplementacion consultarMascotasRepositorioImplementacion;

    @Mock
    IConsultarMascotasCRUD consultarMascotasCRUD;

    @Test
    void consultarMascotas_ExistenMascotas_DevuelveListaConMascotas() {
        when(consultarMascotasCRUD.findAll()).thenReturn(FactoryMascotaAdapter.sampleManyEntity(3));
        List<Mascota> mascotas = (List<Mascota>) consultarMascotasRepositorioImplementacion.obtenerMascotas();
        assertEquals(3, mascotas.size());
    }

    @Test
    void consultarMascotas_NoExistenMascotas_DevuelveListaVacia() {
        when(consultarMascotasCRUD.findAll()).thenReturn(Collections.emptyList());
        List<Mascota> mascotas = (List<Mascota>) consultarMascotasRepositorioImplementacion.obtenerMascotas();
        assertEquals(0, mascotas.size());
    }

}
