package ar.edu.undec.adapter.service.mascota;

import ar.edu.undec.adapter.factory.FactoryMascotaAdapter;
import ar.edu.undec.adapter.service.mascota.controller.ConsultarMascotasController;
import ar.edu.undec.adapter.service.mascota.model.MascotaDTO;
import mascota.input.IConsultarMascotasInput;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ConsultarMascotasServiceTest {

    @InjectMocks
    ConsultarMascotasController consultarMascotasController;

    @Mock
    IConsultarMascotasInput consultarMascotasInput;

    @BeforeEach
    void setup() {
        consultarMascotasController = new ConsultarMascotasController(consultarMascotasInput);
    }

    @Test
    void consultarMascotas_ExistenMascotas_Devuelve200() {
        when(consultarMascotasInput.consultarMascotas()).thenReturn(FactoryMascotaAdapter.sampleManyCore(3));
        ResponseEntity<List<MascotaDTO>> response = consultarMascotasController.consultarMascotas();
        assertEquals(3, Objects.requireNonNull(response.getBody()).size());
        assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    void consultarMascotas_NoExistenMascotas_Devuelve204() {
        when(consultarMascotasInput.consultarMascotas()).thenReturn(Collections.emptyList());
        ResponseEntity<List<MascotaDTO>> response = consultarMascotasController.consultarMascotas();
        assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
    }
}
