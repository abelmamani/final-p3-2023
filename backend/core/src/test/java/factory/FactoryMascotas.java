package factory;

import mascota.exception.MascotaIncompletaException;
import mascota.modelo.Mascota;

import java.time.LocalDate;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class FactoryMascotas {

    private FactoryMascotas() {
    }

    public static Mascota sample(Integer id) {
        return Mascota.instancia(id, "max" + id, LocalDate.MIN);
    }

    public static Collection<Mascota> sampleMany(Integer cantidad) {
        return IntStream.range(0, cantidad).mapToObj(FactoryMascotas::sample).collect(Collectors.toList());
    }
}
