package usecase;

import mascota.exception.MascotaExisteException;
import mascota.exception.MascotaIncompletaException;
import mascota.input.ICrearMascotaInput;
import mascota.modelo.Mascota;
import mascota.output.ICrearMascotaRepositorio;
import mascota.usecase.CrearMascotaCasoUso;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CrearMascotaUseCaseTest {

    ICrearMascotaInput crearMascotaInput;

    @Mock
    ICrearMascotaRepositorio crearMascotaRepositorio;

    @BeforeEach
    void setup() {
        crearMascotaInput = new CrearMascotaCasoUso(crearMascotaRepositorio);
    }

    @Test
    public void crearMascota_noExisteMascota_CreaCorrectamente() throws MascotaIncompletaException, MascotaExisteException {
        Mascota nuevaMascota = Mascota.instancia(null, "max", LocalDate.of(2020, 1, 1));
        CrearMascotaCasoUso crearMascotaCasoUso = new CrearMascotaCasoUso(crearMascotaRepositorio);


        when(crearMascotaRepositorio.existePorNombre(nuevaMascota.getNombre())).thenReturn(false);
        when(crearMascotaRepositorio.guardar(nuevaMascota)).thenReturn(1);


        Integer resultado = crearMascotaCasoUso.crearMascota(nuevaMascota);


        Assertions.assertEquals(1,resultado);
    }

    @Test
    public void crearMascota_ExisteMascota_MascotaExisteException() throws MascotaIncompletaException {
        Mascota nuevaMascota = Mascota.instancia(null, "max", LocalDate.of(2020, 1, 1));
        when(crearMascotaRepositorio.existePorNombre("max")).thenReturn(true);
        verify(crearMascotaRepositorio, never()).guardar(nuevaMascota);
        Exception exception = assertThrows(MascotaExisteException.class, () -> crearMascotaInput.crearMascota(nuevaMascota));
        assertEquals("Ya existe una mascota con nombre max", exception.getMessage());
    }
}
