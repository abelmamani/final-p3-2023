package mascota.exception;

public class MascotaExisteException extends RuntimeException{
    public MascotaExisteException(String msg){
        super(msg);
    }
}
