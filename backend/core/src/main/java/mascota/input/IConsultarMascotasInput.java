package mascota.input;

import mascota.modelo.Mascota;

import java.util.Collection;

public interface IConsultarMascotasInput {
    Collection<Mascota> consultarMascotas();
}
