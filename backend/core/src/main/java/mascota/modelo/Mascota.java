package mascota.modelo;

import mascota.exception.MascotaIncompletaException;
import mascota.exception.MascotaIncorrectaException;

import java.time.LocalDate;

public class Mascota {
    private Integer id;
    private String nombre;
    private LocalDate fechaNacimiento;

    private Mascota(Integer id, String nombre, LocalDate fechaNacimiento) {
        this.id = id;
        this.nombre=nombre;
        this.fechaNacimiento = fechaNacimiento;
    }
    public static Mascota instancia(Integer id, String nombre, LocalDate fechaNacimiento) {
        if(nombre == null || nombre.isBlank())
            throw new MascotaIncompletaException("El nombre de la mascota es obligatorio");
        if(fechaNacimiento == null)
            throw new MascotaIncompletaException("La fecha de nacimiento de la mascota es obligatoria");
        if(fechaNacimiento.isAfter(LocalDate.now()))
            throw new MascotaIncorrectaException("La fecha de nacimiento de la mascota no puede ser superior a la actual");
        return new Mascota(id, nombre, fechaNacimiento);
    }

    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }
}
