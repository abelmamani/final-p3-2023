package mascota.output;

import mascota.modelo.Mascota;

import java.util.Collection;

public interface IConsultarMascotasRepositorio {
    Collection<Mascota> obtenerMascotas();
}
