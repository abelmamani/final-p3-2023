package mascota.usecase;

import mascota.input.IConsultarMascotasInput;
import mascota.modelo.Mascota;
import mascota.output.IConsultarMascotasRepositorio;

import java.util.Collection;

public class ConsultarMascotaUseCase implements IConsultarMascotasInput {
    private IConsultarMascotasRepositorio iConsultarMascotasRepositorio;

    public ConsultarMascotaUseCase(IConsultarMascotasRepositorio iConsultarMascotasRepositorio) {
        this.iConsultarMascotasRepositorio = iConsultarMascotasRepositorio;
    }

    @Override
    public Collection<Mascota> consultarMascotas() {
        return iConsultarMascotasRepositorio.obtenerMascotas();
    }
}
