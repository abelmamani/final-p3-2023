package mascota.usecase;

import mascota.exception.MascotaExisteException;
import mascota.input.ICrearMascotaInput;
import mascota.modelo.Mascota;
import mascota.output.ICrearMascotaRepositorio;

public class CrearMascotaCasoUso implements ICrearMascotaInput {
    private ICrearMascotaRepositorio crearMascotaRepositorio;

    public CrearMascotaCasoUso(ICrearMascotaRepositorio crearMascotaRepositorio) {
        this.crearMascotaRepositorio = crearMascotaRepositorio;
    }

    @Override
    public Integer crearMascota(Mascota mascota) {
        if(crearMascotaRepositorio.existePorNombre(mascota.getNombre()))
            throw new MascotaExisteException("Ya existe una mascota con nombre "+mascota.getNombre());
        return crearMascotaRepositorio.guardar(mascota);
    }
}
